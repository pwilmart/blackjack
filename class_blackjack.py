import random
import os
from tkinter import *

class Blackjack():

    def __init__(self):
        self.csymbole = ["pique", "trefle", "carreau", "coeur"]
        self.cvaleur = ["as","2","3","4","5","6","7","8","9","10", "valet", "dame", "roi"]

    def createDeck(self):
        self.deck = []
        for i in self.csymbole:
            for j in self.cvaleur:
                ctemp = i,j
                self.deck.append(ctemp)
        return self.deck

    def distribution(self,nbr,deck):
        self.dist = []
        x = 0
        while x < nbr:
            ctemp = random.choice(self.deck)
            self.dist.append(ctemp)
            deck.remove(ctemp)
            x+=1

        return self.dist

    def testValeurs(self,d,somme):
        self.csymbole = ["valet","dame","roi"]
        for i in d:
            if i[1] in self.csymbole:
                somme = somme + 10
            elif i[1] == "as":
                if somme <= 11:
                    somme = somme + 10
                else:
                    somme = somme + 1
            else:
                somme = somme + int(i[1])
        return somme

    def checkSomme(self, sommeJoueur,sommeBanque,miseJoueur,fenetre,cadreInfos):
        if sommeJoueur == 21:
            miseJoueur = miseJoueur * 3
            Label(cadreInfos, text="Bravo, vous avez gagné \n" +
                                   "Vous gagnez 3 fois votre mise soit un total de: " +
                                   str(miseJoueur)).pack(padx=50, pady=50)

        elif sommeJoueur > sommeBanque and sommeJoueur < 22:
            miseJoueur = miseJoueur * 2
            Label(cadreInfos,
                  text="Bravo, vous avez gagné \n" + "Vous gagnez 2 fois votre mise soit un total de: " + str(
                      miseJoueur)).pack(padx=50, pady=50)

        elif sommeJoueur > 21 or sommeJoueur < sommeBanque and sommeBanque < 21:
            miseJoueur = 0
            print(miseJoueur)
            Label(cadreInfos, text="Vous avez perdu \n Votre mise est à : " + str(miseJoueur)).pack(padx=50, pady=50)

        elif sommeJoueur == sommeBanque:
            miseJoueur = miseJoueur
            Label(cadreInfos,
                  text="Egalité avec la banque \n Vous récupérer votre mise soit un total de " + str(miseJoueur)).pack(
                padx=50, pady=50)

        elif sommeBanque == 21:
            Label(cadreInfos, text="Blackjack de la banque \n" + "Vous avez perdu vous perdez votre mise").pack(padx=50,
                                                                                                                pady=50)
            miseJoueur = 0
        return sommeJoueur, miseJoueur